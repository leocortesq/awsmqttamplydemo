import React from "react";
import { Card, CardHeader, CardText, CardBody, Row, Col } from "reactstrap";

const MessageCardBasic = (message) => {
  const topic = message.mess[Object.getOwnPropertySymbols(message.mess)[0]];
  const mess = message.mess.message;
  return (
    <Card>
      <CardHeader>{topic}</CardHeader>
      <CardBody>
        <CardText>{mess}</CardText>
      </CardBody>
    </Card>
  );
};

const MessageCard = (message) => {
  const topic = message.mess[Object.getOwnPropertySymbols(message.mess)[0]];
  const mess = message.mess.message;
  return (
    <Card className="shadow1">
      <CardHeader>{topic}</CardHeader>
      <CardBody>
        <CardText>{mess}</CardText>
      </CardBody>
    </Card>
  );
};

const MessagesMap = (props) => {
  return (
    <Row xs="1" md="3">
      {props.messages.map((message, index) => {
        return (
          <Col key={index}>
            <MessageCard mess={message} key={index} />
          </Col>
        );
      })}
    </Row>
  );
};

export default MessagesMap;
