import React from "react";

const SubscribesMap = (props) =>
  props.topics.map((topic, index) => <p key={index}>{topic}</p>);

export default SubscribesMap;
