import React from "react";
import {
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
} from "reactstrap";

class PubForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { topic: "", value: "" };
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClick = (e) => {
    e.preventDefault();
    this.props.handlePub(this.state.topic, this.state.value);
    console.log("desde form: " + this.state.topic + ", " + this.state.value);
  };

  render() {
    return (
      <Form onSubmit={this.handleClick}>
        <FormGroup row>
          <Col xs={3}>
            <Label>Tema:</Label>
          </Col>
          <Col xs={8}>
            <Input
              type="text"
              name="topic"
              id="topic"
              value={this.state.topic}
              onChange={this.handleChange}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Label>Mensaje</Label>
          <Input
            type="textarea"
            name="value"
            id="value"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <Button onClick={this.handleClick} className="btn-block btn-green">
            Publicar
          </Button>
        </FormGroup>
      </Form>
    );
  }
}
export default PubForm;
