import React, { Component } from "react";
import "../Menu.css";
import { slide as Menu } from "react-burger-menu";
import { Button } from "reactstrap";
//import MyForm from "./SubscribeForm";
import SubForm from "./SubForm";
import PubForm from "./PubForm";

import SubscribesMap from "./SubscribesMap";
//import { Nav } from "reactstrap";

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSub = (value) => {
    this.props.handleSub(value);
  };

  handleUnSub = (value) => {
    this.props.handleUnSub();
  };

  handlePub = (topic, value) => {
    this.props.handlePub(topic, value);
  };

  //<MyForm handleSubmit={this.props.handleSubmit} />
  render() {
    // NOTE: You also need to provide styles, see https://github.com/negomi/react-burger-menu#styling
    return (
      <Menu isOpen>
        <h3>Publicar</h3>
        <PubForm handlePub={this.handlePub} />
        <hr />
        <h3>Susbscribirse</h3>
        <SubForm handleSub={this.handleSub} />
        <SubscribesMap topics={this.props.topics} />
        <hr />
        <h3>UnSub</h3>
        <Button onClick={this.props.handleUnSub} className="btn-block btn-red">
          UnSubscribe
        </Button>
      </Menu>
    );
  }
}

export default Navigation;
