import React from "react";
import {
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
} from "reactstrap";

class SubForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  };
  handleClick = (e) => {
    e.preventDefault();
    this.props.handleSub(this.state.value);
  };

  render() {
    return (
      <Form onSubmit={this.handleClick}>
        <FormGroup row>
          <Col xs={8}>
            <Input
              type="text"
              name="topicsubscribe"
              id="topicsubscribe"
              value={this.state.value}
              onChange={this.handleChange}
            />
          </Col>
          <Button onClick={this.handleClick}>Sub</Button>
        </FormGroup>
      </Form>
    );
  }
}
export default SubForm;
