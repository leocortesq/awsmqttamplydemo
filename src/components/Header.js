import React from "react";
import { Jumbotron, Container } from "reactstrap";

const Header = (props) => {
  return (
    <div>
      <Jumbotron fluid>
        <Container fluid>
          <h1 className="display-3">DEMO AWS IoT CORE</h1>
          <p className="lead">
            Esta sección del demo muestra los mensajes recibidos de topic test/#
          </p>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default Header;
