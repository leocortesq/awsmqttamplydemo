import React, { Component } from "react";
import "./App.css";
import Amplify from "aws-amplify";
import { AWSIoTProvider } from "@aws-amplify/pubsub/lib/Providers";
import { Container } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "./components/Header";
import MessagesMap from "./components/MessagesMap";
import Navigation from "./components/Navigation";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topics: [],
      messages: [],
    };
    let subscription;
  }

  componentDidMount() {
    console.log("did mount.");
    Amplify.configure({
      Auth: {
        identityPoolId: "us-east-2:591ebb99-5f0c-4ea7-b480-3639d653ca6d",
        region: "us-east-2",
        userPoolId: "us-east-2_TjD94lszx",
        userPoolWebClientId: "7rg1sdgvqlolf8ini946538bka",
      },
    });

    //aws_pubsub_endpoint: `wss://${process.env.REACT_APP_MQTT_ID}.iot.${process.env.REACT_APP_REGION}.amazonaws.com/mqtt`,
    Amplify.addPluggable(
      new AWSIoTProvider({
        aws_pubsub_region: "us-east-2",
        aws_pubsub_endpoint: `wss://ad253x8vn773o-ats.iot.us-east-2.amazonaws.com/mqtt`,
      })
    );

    /*Amplify.PubSub.subscribe("test/#").subscribe({
      next: (data) => console.log("Message received", data),
      error: (error) => console.error(error),
      close: () => console.log("Done"),
    });*/

    /*Amplify.PubSub.subscribe("test/#").subscribe({
      next: (data) => this.addMessage(data.value),
      error: (error) => console.error(error),
      close: () => console.log("Done"),
    });*/
  }

  handleSub = (value) => {
    this.state.topics.includes(value)
      ? this.notify("Ya se encuentra subscrito a al tema.", "warning")
      : this.setState({ topics: [...this.state.topics, value] }, () => {
          if (this.subscription) {
            this.subscription.unsubscribe();
          }
          this.subscription = Amplify.PubSub.subscribe([
            ...this.state.topics,
          ]).subscribe({
            next: (data) =>
              this.setState({ messages: [...this.state.messages, data.value] }),
            error: (error) => console.error(error),
            close: () => console.log("Done"),
          });
        });
  };

  handleUnSub = () => {
    if (this.subscription) {
      console.log("unsubscribe method");
      this.subscription.unsubscribe();
      this.setState({ topics: [] });
      this.notify(
        "Ya no se encuentra subscrito a los temas del broker. Debe volver a realizar el proceso de subscripción.",
        "dark"
      );
    }
  };

  handlePub = (topic, value) => {
    //this.notify("Publicando mensaje", "info");
    this.publishAsync(topic, value)
      .then(() => {
        this.notify("Mensaje publicado", "success");
      })
      .catch(() => {
        this.notify("Error al publica mensaje", "error");
      });
  };

  publishAsync = async (topic, value) =>
    Amplify.PubSub.publish(topic, {
      message: value,
    });

  notify = (value, typeToast) =>
    toast(value, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      type: typeToast,
    });

  render() {
    console.log(this.state.topics);
    console.log([...this.state.topics]);
    return (
      <div id="outer-container">
        <div className="App">
          <Navigation
            handlePub={this.handlePub}
            handleSub={this.handleSub}
            topics={this.state.topics}
            handleUnSub={this.handleUnSub}
          />
          <Header />
          <Container>
            <MessagesMap messages={this.state.messages} />
          </Container>
          <ToastContainer />
        </div>
      </div>
    );
  }
}

export default App;
